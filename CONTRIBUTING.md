<h1>Kontribusi</h1>
<p>Jika Anda ingin berkontribusi, Anda bisa meng-fork repositori ini dan menambahkan dokumentasi atau tutorial-tutorial dan melakukan pull request. Atau Anda juga bisa mengirimkan file dokumentasi atau tutorial ke email saya : ditokurniap@merahputih.id.</p>
<p>Tutorial-tutorial atau dokumentasi yang dikirim ke sini akan di lisensi kan ke dalam CC By-SA 4.0 dengan nama pembuat tutorial atau dokumentasi Anda sendiri, bukan saya namanya karena dikirim oleh Anda.</p>
